<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Nugunee : Main</title>

<link rel="stylesheet" href="<c:url value='/resources/assets/plugins/bootstrap/css/bootstrap.min.css'/>">
<link rel="stylesheet" href="<c:url value='/resources/assets/css/custom.css'/>">


<script type="text/javascript" src="<c:url value='/resources/assets/plugins/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/jquery-migrate-1.2.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
<!-- JS Implementing Plugins -->           
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/back-to-top.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/countdown/jquery.countdown.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/backstretch/jquery.backstretch.min.js'/>"></script>


<script>
var click_count=1;
$(document).ready(function(){
	
	console.log(click_count);
		$.ajax({
			url:"boardList",
			type:"post",
			dataType:"json",
			data:"contentNo="+click_count,
			success:function(getData){
				console.log(getData)
				var result="";		
				getData.boardList.sort(function(a,b){
					var x = a.contentNo > b.contentNo ? -1: 1;
					return x;
				});				
				$(getData.boardList).each(function(index,item){
					console.log("게시판 번호 : "+item.contentNo);
					result+='<div class="panel panel-default"><div class="panel-heading"><a href="deleteBoard?contentNo='+item.contentNo+'"><span class="glyphicon glyphicon-remove pull-right"></span></a><a href="#" class="pull-right">전체 공개&nbsp;&nbsp;</a> <h4>';
	                result+=item.memberNickname;
	                result+='</h4></div><div class="panel-body"><span class="pull-right">';
	                result+=item.date;
	                result+='</span><a href="#">';
	                result+=item.content;
	                result+='<div class="clearfix"></div><hr><div class="hashtag"></div><form><div class="input-group"><div class="input-group-btn"><button class="btn btn-default">좋아요♥</button><button class="btn btn-default">공유하기</button>';
	                result+='</div><input type="text" class="form-control" placeholder="댓글을 입력하세요..."></div></form></div></div>';
				});
				$(".col-sm-7").html(result);
			},
			error:function(err){
				alert(err+"에러 발생");
			}
		}); //ajax end
		
		/* $(document).on("click",".media-body",function(){
			$("#modal_Login").modal('show');
		}); */
		
		
		
		$(".search").keyup(function(){
			if($(this).val ==""){
				$(".col-sm-7").hide();
					return;
				}
			$.ajax({
				url:"searchBox",
				data:"word="+$(this).val(),
				type:"post",
				dataType:"text",
				success:function(getData){
					console.log(getData);
					getData = JSON.parse(getData);
					console.log(getData.size+"겟데이터 리스트 사이즈");
					if(getData.size > 0){
						var result="";
						$(getData.list).each(function(index,item){
							console.log("게시판 번호 : "+item.contentNo);
							result+='<div class="panel panel-default"><div class="panel-heading"><a href="deleteBoard?contentNo='+item.contentNo+'"><span class="glyphicon glyphicon-remove pull-right"></span></a><a href="#" class="pull-right">전체 공개&nbsp;&nbsp;</a> <h4>';
			                result+=item.memberNickname;
			                result+='</h4></div><div class="panel-body"><span class="pull-right">';
			                result+=item.date;
			                result+='</span><a href="#">';
			                result+=item.content;
			                result+='<div class="clearfix"></div><hr><div class="hashtag"></div><form><div class="input-group"><div class="input-group-btn"><button class="btn btn-default">좋아요♥</button><button class="btn btn-default">공유하기</button>';
			                result+='</div><input type="text" class="form-control" placeholder="댓글을 입력하세요..."></div></form></div></div>';
						});
						$(".col-sm-7").html(result);
						$(".col-sm-7").show();
					}else{
						$(".col-sm-7").hide();
					}
				},
				error:function(err){
					alert(err+"오류 발생");
				}
			});
		});
});





</script>

</head>
<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
            <!-- main right col -->
            <div class="column col-sm-12 col-xs-12" id="main">
                
                <!-- top nav -->
              	<div class="navbar navbar-blue navbar-static-top">  
                    <div class="navbar-header">
                      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
          				<span class="icon-bar"></span>
          				<span class="icon-bar"></span>
                      </button>
                      <a href="/" class="navbar-brand logo">Nugunee</a>
                  	</div>
                  	<nav class="collapse navbar-collapse" role="navigation">
                    <form class="navbar-form navbar-left">
                        <div class="input-group input-group-sm" style="max-width:360px;">
                          <input type="text" class="form-control search" placeholder="궁금한 친구나 장소를 검색해보세요." name="srch-term" id="srch-term">
                          <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                          </div>
                        </div>
                    </form>
                    <ul class="nav navbar-nav">
                      
                      <li>
                        <a href="#postModal" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> 게시</a>
                      </li>
                      <li>
                        <a href="#"><span class="badge">알림</span></a>
                      </li>
                      
                    </ul>
                    <ul class="nav navbar-nav navbar-right">                    	
                      <li><a href="#"><i class="glyphicon glyphicon-user"></i></a></li>
                      <li><a href="logout">로그아웃</a></li>
                        
                      
                    </ul>
                  	</nav>
                </div>
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->                      
                      	<div class="row">
                          
                         <!-- main col left --> 
                         <div class="col-sm-5">
                           
                              <div class="panel panel-default">
                                <div class="panel-thumbnail"><img src="http://www.bootply.com/assets/example/bg_5.jpg" class="img-responsive"></div>
                                <div class="panel-body">
                                  <p class="lead"><a href="#">오늘의 닉네임 : ${anonyNick}</a></p>
                                  <p>45명 팔로워, ${countBoard}개의 글</p>
                                  
                                </div>
                              </div>
                           
                          </div>
                          
                          <!-- main col right -->
                          <div class="col-sm-7">
                               
                               <div class="panel panel-default">
                                 <div class="panel-heading"><a href="#" class="pull-right">전체 공개</a> <h4>착실한 향유고래</h4></div>
                                  <div class="panel-body">
                                  	<span class="pull-right">3월 24일 오후 6:06</span>
                                    <a href="#">수지가 아까울까 이민호가 아까울까 <br/>시간이 아까웠다</a></p>
                                    <div class="clearfix"></div>
                                    <hr>
                                    #Dispatch #디스패치
                                    <hr>
                                    <form>
                                    <div class="input-group">
                                      <div class="input-group-btn">
                                      <button class="btn btn-default">좋아요♥</button><button class="btn btn-default">공유하기</button>
                                      </div>
                                      <input type="text" class="form-control" placeholder="댓글을 입력하세요...">
                                    </div>
                                    </form>
                                  </div>
                               </div>
                               
                               <div class="panel panel-default">
                                 <div class="panel-heading"><a href="#" class="pull-right">비공개</a> <h4>살벌한 사슴벌레</h4></div>
                                  <div class="panel-body">
                                  	<span class="pull-right">3월 23일 오후 5:06</span>
                                    <a href="#">언니 저 마음에 안들죠?</a></p>
                                    <div class="clearfix"></div>
                                    <hr>
                                    #Dispatch #디스패치 #예원
                                    <hr>
                                    <form>
                                    <div class="input-group">
                                      <div class="input-group-btn">
                                      <button class="btn btn-default">좋아요♥</button><button class="btn btn-default">공유하기</button>
                                      </div>
                                      <input type="text" class="form-control" placeholder="댓글을 입력하세요...">
                                    </div>
                                    </form>
                                  </div>
                               </div>
                          </div>
                          <div class="LoadMore">
                               	<button class="btn btn-primary btn-lg btn-block">더 읽기</button>	
                          </div>
                          
                          
                       </div><!--/row-->
                      
                        
                      
                        <div class="row" id="footer">    
                          <div class="col-sm-6">
                            
                          </div>
                          <div class="col-sm-6">
                            <p>
                            <a href="#" class="pull-right">©Copyright 2015</a>
                            </p>
                          </div>
                        </div>
                      
                      <hr>
                        
                      
                    </div><!-- /col-9 -->
                </div><!-- /padding -->
            </div>
            <!-- /main -->
          
        </div>
    </div>
</div>


<!--post modal-->
<div id="postModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			익명닉네임으로 글 작성하기
      </div>
      <div class="modal-body">
          <form class="form center-block">
            <div class="form-group">
              <textarea class="form-control input-lg content" autofocus="" placeholder="지금 무슨 생각을 하고 계신가요?"></textarea>
            </div>
          </form>
      </div>
      <div class="modal-footer">
          <div>
          <button class="btn btn-primary btn-sm posting" data-dismiss="modal" aria-hidden="true">글 등록</button>
            
		  </div>	
      </div>
  </div>
  </div>
</div>
<script>

$(document).on("click",".posting",function(){
	$.ajax({
		url:"insertBoard", 
		type:"post",
		dataType:"text",
		data:"content="+$(".content").val(),
		success:function(EnrollResult){
			
			alert("등록 되었습니다."+EnrollResult);
			location.href="main";
		},
		error:function(err){
			alert("글 등록 실패하였습니다.");
		}
	}); 
});


$(".LoadMore").click(function(){
	
	click_count+=4;
	console.log(click_count);
	$.ajax({
		url:"loadMore",
		type:"post",
		dataType:"json",
		data:"contentNo="+click_count,
		success:function(getData){
			console.log(getData)
			var result="";		
			getData.boardList.sort(function(a,b){
				var x = a.contentNo > b.contentNo ? -1: 1;
				return x;
			});				//scrollpoint sp-effect1 active animated fadeInLeft
			$(getData.boardList).each(function(index,item){
				result+='<div class="scrollpoint sp-effect1 active animated fadeInLeft"><div class="panel panel-default"><div class="panel-heading"><span class="glyphicon glyphicon-remove pull-right"></span><a href="#" class="pull-right">전체 공개&nbsp;&nbsp;</a><h4>';
                result+=item.memberNickname;
                result+='</h4></div><div class="panel-body"><span class="pull-right">';
                result+=item.date;
                result+='</span><a href="#">';
                result+=item.content;
                result+='<div class="clearfix"></div><hr><div class="hashtag"></div><form><div class="input-group"><div class="input-group-btn"><button class="btn btn-default">좋아요♥</button><button class="btn btn-default">공유하기</button>';
                result+='</div><input type="text" class="form-control" placeholder="댓글을 입력하세요..."></div></form></div></div></div>';
			});
			$(".col-sm-7").append(result);
		},
		error:function(err){
			alert(err+"에러 발생");
		}
	})
});






/* off-canvas sidebar toggle */
$('[data-toggle=offcanvas]').click(function() {
  	$(this).toggleClass('visible-xs text-center');
    $(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
    $('.row-offcanvas').toggleClass('active');
    $('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
    $('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');
    $('#btnShow').toggle();
});

</script>
	</body>
</html>