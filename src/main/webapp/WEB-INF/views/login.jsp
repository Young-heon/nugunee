<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="ko">  
<head>
    <title>Nugunee : Login Page..</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="<c:url value='/resources/assets/plugins/bootstrap/css/bootstrap.min.css'/>">
    <link rel="stylesheet" href="<c:url value='/resources/assets/css/style.css'/>">
    

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="<c:url value='/resources/assets/plugins/line-icons/line-icons.css'/>">
    <link rel="stylesheet" href="<c:url value='/resources/assets/plugins/font-awesome/css/font-awesome.min.css'/>">

    <!-- CSS Page Style -->    
    <link rel="stylesheet" href="<c:url value='/resources/assets/css/pages/page_log_reg_v2.css'/>">    

    <!-- CSS Theme -->    
    <link rel="stylesheet" href="<c:url value='/resources/assets/css/themes/blue.css" id="style_color'/>">

    <!-- CSS Customization -->
    
    <link rel="stylesheet" href="<c:url value='/resources/assets/css/custom.css'/>">
    
    <style>
.reg-block{
height: 440px;

}
body{
background-color:#2980b9;
}



.logo {
  display:block;
  padding:3px;
  background-color:#fff;
  color:#2980b9;
  height:70px;
  width:300px;
  margin:9px;
  margin-right:2px;
  margin-left:15px;
  font-size:40px;
  font-weight:900;
  text-align:center;
  text-decoration:none;
  text-shadow:0 0 1px;
  border-radius:2px;

}

</style>
    
</head> 

<body>
<!--=== Content Part ===-->    
<div class="container">
    <!--Reg Block-->
    <div class="reg-block">
        <div class="reg-block-header">
            <span class="logo">Nugunee</span>
            <ul class="social-icons text-center">
                <li><a class="rounded-x social_facebook" data-original-title="Facebook" href="#"></a></li>
                <li><a class="rounded-x social_twitter" data-original-title="Twitter" href="#"></a></li>
                <li><a class="rounded-x social_googleplus" data-original-title="Google Plus" href="#"></a></li>
                <li><a class="rounded-x social_linkedin" data-original-title="Linkedin" href="#"></a></li>
            </ul>
            <p>Don't Have Account? Click <a class="color-green" href="#">Sign Up</a> to registration.</p>            
        </div>

        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <input type="text" class="form-control" placeholder="Email" id="memberEmail" name="memberEmail" required>
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" class="form-control" placeholder="Password" id="memberPasswd" name="memberPasswd" required>
        </div>
        <hr>
        <label class="checkbox">
            <input type="checkbox"> 
            <p>Always stay signed in</p>
        </label>
                                
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <button type="button" class="btn-u btn-block" id="loginButton" name="loginButton">Log In</button>
            </div>
        </div>
    </div>
    <!--End Reg Block-->
</div><!--/container-->
<!--=== End Content Part ===-->

<%-- <script type="text/javascript" src="<c:url value='/js/jquery-1.11.1.min.js'/>"></script> --%>
<!-- JS Global Compulsory -->           

<script type="text/javascript" src="<c:url value='/resources/assets/plugins/jquery-1.10.2.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/jquery-migrate-1.2.1.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/bootstrap/js/bootstrap.min.js'/>"></script> 
<!-- JS Implementing Plugins -->           
<!-- <script type="text/javascript" src="<c:url value='/resources/assets/plugins/back-to-top.js'/>"></script>---->
<!-- <script type="text/javascript" src="<c:url value='/resources/assets/plugins/countdown/jquery.countdown.js'/>"></script>-->
<script type="text/javascript" src="<c:url value='/resources/assets/plugins/backstretch/jquery.backstretch.min.js'/>"></script>
<!-- <script type="text/javascript">
    $.backstretch([
      "assets/img/bg/5.jpg",
      "assets/img/bg/4.jpg",
      ], {
        fade: 1000,
        duration: 7000
    });
</script> -->



<script>

		$('#loginButton').click(function(){  //값 전송하여 로그인 체크 필요
			console.log($('#memberEmail').val())
			console.log($('#memberPasswd').val())
		var fromData = {
				'memberEmail' : $('#memberEmail').val(),
				'memberPwd' : $('#memberPasswd').val()
				
		}
		console.log(fromData);
		$.ajax({
			url:"loginCheck",
			type:"post",
			dataType:"text",
			data:fromData,
			success:function(result){
				
				if(result>0){
					
					location.href="main";
					
				}else{
					alert("로그인 실패");
				}
			},
			error:function(err){
				alert("로그인 실패함");
			}
		})
	});
	
	
</script>

<!-- JS Page Level -->           
<script type="text/javascript" src="<c:url value='/resources/assets/js/app.js'/>"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
    });
</script>
</body>
</html> 