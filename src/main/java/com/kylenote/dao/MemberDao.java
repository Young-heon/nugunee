package com.kylenote.dao;

import java.util.List;

import com.kylenote.vo.BoardVo;
import com.kylenote.vo.MemberVo;

public interface MemberDao {

	MemberVo loginCheck(MemberVo memberVo);

	List<BoardVo> boardList(int contentNo);

	int insertBoard(BoardVo vo);
	
	public BoardVo boardOneList(int contentNo);
	
	public int deleteBoard(int contentNo);
	
	public int countBoard(int memberNo);
	
	public List<BoardVo> suggest(String content);
}
