package com.kylenote.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kylenote.vo.BoardVo;
import com.kylenote.vo.MemberVo;

@Repository
public class MemberDaoImpl implements MemberDao {

	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public MemberVo loginCheck(MemberVo memberVo) {
		
		
		return sqlSession.selectOne("member.loginCheck",memberVo);
	}

	@Override
	public List<BoardVo> boardList(int contentNo) {
		
		System.out.println("DAO "+ contentNo);
		
		
		
		List<BoardVo> list = sqlSession.selectList("member.boardList", contentNo);
		
		System.out.println(list.size());
		System.out.println(list.get(1).getContent());
		return sqlSession.selectList("member.boardList",contentNo);
	}

	@Override
	public int insertBoard(BoardVo vo) {
		
		return sqlSession.insert("member.insertBoard", vo);
	}
	
	@Override
	public BoardVo boardOneList(int contentNo){
		return sqlSession.selectOne("member.boardOne",contentNo);
	}
	
	@Override
	public int deleteBoard(int contentNo){
		return sqlSession.delete("member.deleteBoard",contentNo);
	}
	
	@Override
	public int countBoard(int memberNo){
		return sqlSession.selectOne("member.countBoard",memberNo);
	}
	
	@Override
	public List<BoardVo> suggest(String content){
		return sqlSession.selectList("member.suggest",content);
	}

}
