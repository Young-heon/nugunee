package com.kylenote.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kylenote.service.MemberService;
import com.kylenote.vo.BoardVo;
import com.kylenote.vo.MemberVo;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	MemberService memberService;
	
	@RequestMapping("loginCheck")
	@ResponseBody
	public String loginCheck(MemberVo memberVo, HttpSession session, HttpServletRequest request) throws Exception{
		String flag="0";
		System.out.println(memberVo.getMemberEmail()+", "+memberVo.getMemberPwd());
		MemberVo vo = memberService.loginCheck(memberVo);
		if(vo==null){
			try {
				throw new Exception("정보가 일치하지 않습니다.");
			} catch (Exception e) {
				
				request.setAttribute("errorMsg", e.getMessage());
				throw new Exception();
			}
		}else{
			flag="1";
			session.setAttribute("anonyNick", memberService.getAnonymous(vo.getMemberNo()));
			session.setAttribute("memberNo", vo.getMemberNo());
			session.setAttribute("countBoard", memberService.countBoard(vo.getMemberNo()));
			
		}
		
		return flag; 
	}

	//게시글 가져오기
	@RequestMapping(value={"boardList" ,"loadMore"})
	public ModelAndView boardList(@RequestParam int contentNo){
		System.out.println("ContentNo : "+contentNo);
		
		ModelAndView mv = new ModelAndView();
		List<BoardVo> list = memberService.boardList(contentNo);
		System.out.println("list 완료");
		
		mv.addObject("boardList", list);
		mv.setViewName("jsonView");
		
		System.out.println("ModelAndView");
		return mv;
	}
	
	//게시글 삭제하기
	@RequestMapping("deleteBoard")
	public String deleteBoard(@RequestParam int contentNo, HttpSession session){
		String location="error";
		int memberNo = (int)session.getAttribute("memberNo");
		System.out.println("접속한 회원번호 : "+memberNo);
		System.out.println("삭제할 게시물번호 : "+contentNo);
		
		BoardVo vo = memberService.boardOneList(contentNo);
		if(vo.getMemberNo() != memberNo){
			//삭제 금지 에러 발생
			//에러 페이지로 이동 또는 알림
			
		}else{
			//삭제 처리
			
			memberService.deleteBoard(contentNo);
			location = "main";
		}
		return location;
	}
	
	
	
	//글 등록하기
	@RequestMapping("insertBoard")
	@ResponseBody
	public String insertBoard(HttpSession session, @RequestParam String content){
		
		System.out.println(session.getAttribute("anonyNick"));
		System.out.println(session.getAttribute("memberNo"));
		System.out.println("content : "+content);
		content = content.replace("\n","<br/>");
		BoardVo vo = new BoardVo();
		
		vo.setMemberNickname((String)session.getAttribute("anonyNick"));
		vo.setContent(content);
		vo.setMemberNo((int) session.getAttribute("memberNo"));
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM월 dd일 a hh:mm",Locale.KOREA);
		Date today = new Date();
		
		vo.setDate(sdf.format(today));
		int flag = memberService.insertBoard(vo);
		System.out.println(flag);
		
		return flag+"";
	}
	
	@RequestMapping("searchBox")
	public ModelAndView suggest(String word){
		List<BoardVo> list = memberService.suggest(word);
		ModelAndView mv =new ModelAndView();
		mv.addObject("size", list.size());
		mv.addObject("list", list);
		mv.setViewName("jsonView");
	return mv;
	}
	
	@RequestMapping("logout")
	public String logout(HttpSession session){
		
		session.invalidate(); //모든 세션 지우기
		return "login";
	}
	
	
	@RequestMapping("/")
	public String index(){
		return "index"; //WEB-INF/views/index.jsp
	}
	
	@RequestMapping("{url}")
	public void url(){}
	
}
