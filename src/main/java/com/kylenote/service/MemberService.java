package com.kylenote.service;

import java.util.List;

import com.kylenote.vo.BoardVo;
import com.kylenote.vo.MemberVo;

public interface MemberService {
	
	
	public MemberVo loginCheck(MemberVo memberVo);
	
	public String getAnonymous(int no);

	public List<BoardVo> boardList(int contentNo);

	public int insertBoard(BoardVo vo);
	
	public BoardVo boardOneList(int contentNo);
	
	public int deleteBoard(int contentNo);
	
	public int countBoard(int memberNo);
	
	public List<BoardVo> suggest(String content);
	
}
