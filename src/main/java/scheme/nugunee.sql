
select * from tab;


create table member(
	member_no int primary key,
	member_email varchar(100),
	member_passwd varchar(100)
)

create sequence member_no_seq NOCACHE;

select * from member;

insert into member values(member_no_seq.nextval, 'test@test.com', 'test');
insert into member values(member_no_seq.nextval, 'test2@test.com', 'test2');
insert into member values(member_no_seq.nextval, 'test3@test.com', 'test3');
insert into member values(member_no_seq.nextval, 'test4@test.com', 'test4');
insert into member values(member_no_seq.nextval, 'test5@test.com', 'test5');
insert into member values(member_no_seq.nextval, 'test6@test.com', 'test6');

drop table board;

create table board(
	content_no int primary key,
	public_state int,
	write_date varchar(100),
	memberNick varchar(50),
	content varchar(2000),
	member_no int,
	like_no int	
)



create sequence content_no_seq NOCACHE;

insert into board values(content_no_seq.nextval, 0, '3월 24일 오후 6:06', '착실한 향유고래', '수지가 아까울까 이민호가 아까울까 
<br/>시간이 아까웠다',1,0)

insert into board values(content_no_seq.nextval, 0, '3월 22일 오전 5:50', '살벌한 사슴벌레', '언니 저 마음에 안들죠?',1,0)

insert into board values(content_no_seq.nextval, 0, '3월 20일 오전 3:50', '진실한 유럽파랑새', '내가 숲 속에 있는데 어떻게 나무를 안보고 숲을 보나',1,0)

insert into board values(content_no_seq.nextval, 0, '3월 19일 오후 11:50', '쌀쌀한 소노라고퍼뱀', '나만 힘든 건 아니지만<br/> 니가 더 힘든 걸 안다고<br/> 내가 안 힘든 것도 아니다.',1,0)

select * from board


select * from ( select A.*, rownum as rnum from(select * from board order by content_no desc) A where rownum < 1+4) where rnum >=1
